.DEFAULT_GOAL := build

clean: public
	rm -r public

build:
	hugo --gc --minify
