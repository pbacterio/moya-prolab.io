---
title: "Terraform small trick to save a lot disk space in your dev machine"
date: 2020-06-14T08:27:58+02:00
---

## The how

Add this line to your `~/.terraformrc` file:

    plugin_cache_dir = "$HOME/.terraform.d/plugin-cache"

## The why

By default, Terraform download and store the provider plugins inside `.terraform/plugins` at your local project path.

So if you have a good amount of terraform projects, you are wasting disk space (>10G in my case) with mostly copies of the same plugins providers.
And do not mention download time.

Fortunately, HashiCorp already thought on that: [provider-plugin-cache](https://www.terraform.io/docs/configuration/providers.html#provider-plugin-cache)

Enjoy it ;)